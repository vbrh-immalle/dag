﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static int dag = 0;
        static List<string> dagen = new List<string>() { "maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag", "zondag" };

        static void Main(string[] args)
        {
            bool done = false;
            ConsoleKeyInfo invoer = new ConsoleKeyInfo();
            
            // Curiosity what a new ConsoleKeyInfo contains:
            //Console.WriteLine(invoer.Key);
            //Console.WriteLine(invoer.Modifiers);

            while (!done)
            {
                Console.WriteLine();
                PrintMenu();
                invoer = Console.ReadKey(true);
                Console.ForegroundColor = ConsoleColor.White;
                
                if(invoer.Modifiers == 0)
                {
                    Console.WriteLine("{0} ({1})", invoer.Key, DateTime.Now.ToString("yyyy-MM-dd_hh:mm:ss:ffff"));
                } else
                {
                    Console.WriteLine("{0} {1} ({2})", invoer.Modifiers, invoer.Key, DateTime.Now.ToString("yyyy-MM-dd_hh:mm:ss:ffff"));
                }

                Console.ResetColor();
                Console.WriteLine();

                switch (invoer.Key)
                {
                    case ConsoleKey.N:
                        MenuN();
                        break;
                    case ConsoleKey.P:
                        MenuP();
                        break;
                    case ConsoleKey.I:
                        MenuI();
                        break;
                    case ConsoleKey.E:
                        MenuE();
                        break;
                    case ConsoleKey.X:
                        if (invoer.Modifiers.HasFlag(ConsoleModifiers.Control))
                        {
                            done = true;
                        }
                        else
                        {
                            goto default;
                        }
                        break;
                default:
                        MenuDefault();
                        break;
                }
            }

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Thank you! Come again...");
            Console.ResetColor();
        }

        static void MenuN()
        {
            dag = dag + 1;
            if (dag >= 7)
            {
                dag = 0;
            }
            Console.WriteLine("Stap naar volgende dag.");
        }

        static void MenuP()
        {
            dag = dag - 1;
            if (dag < 0)
            {
                dag = 6;
            }
            Console.WriteLine("Stap naar vorige dag.");
        }

        static void MenuI()
        {
            Console.WriteLine("INFO: het is vandaag de {0}e dag, nl. {1}.", dag, dagen[dag]);
        }

        static void MenuE()
        {
            string input = "";
            bool done = false;

            Console.Write("Geef een nieuw dagnummer in (CTRL-X to cancel): ");
            Console.ForegroundColor = ConsoleColor.White;

            ConsoleKeyInfo invoer = new ConsoleKeyInfo();

            // store input
            while(!done)
            {
                invoer = Console.ReadKey(true);
                if (invoer.Modifiers.HasFlag(ConsoleModifiers.Control) && invoer.Key == ConsoleKey.X)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine();
                    Console.WriteLine("Cancel...");
                    Console.ResetColor();
                    return;
                }
                else
                {
                    if(invoer.Key == ConsoleKey.Enter)
                    {
                        done = true;
                    }
                    else if(invoer.Key == ConsoleKey.Backspace)
                    {
                        if(input.Length > 0)
                        {
                            input = input.Substring(0, input.Length - 1);
                            Console.Write("\b \b"); // backspace (non-clearing), space, backspace
                        }
                    }
                    else 
                    {
                        Console.Write(invoer.KeyChar);
                        input += invoer.KeyChar;
                    }
                }
            }

            // parse input
            int dagnummer;
            if(!Int32.TryParse(input, out dagnummer))
            {
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("{0}", input);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(" is geen nummer. Cancel...");
                Console.ResetColor();
            }
            else
            {
                if(dagnummer >= 0 && dagnummer <= 6)
                {
                    dag = dagnummer;
                }
                else
                {
                    Console.WriteLine();
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("{0}", input);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(" is geen geldig dagnummer. Cancel...");
                    Console.ResetColor();
                }
            }
            
        }

        static void MenuDefault()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Keuze bestaat niet! Kies iets anders...");
        }

        static void PrintMenu()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("     N   ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(" Ga naar volgende dag");

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("     P   ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(" Ga naar vorige dag");

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("     I   ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(" Geef info over huidige dag");

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("     E   ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(" Geef rechtstreeks een dag-index in");


            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("  CTRL-X ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(" Exit");

            Console.WriteLine();
            Console.Write("  Maak uw keuze ({0}): ", DateTime.Now.ToString("yyyyMMdd_hh:mm:ss:ffff"));
        }
    }
}
